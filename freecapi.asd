(defsystem :freecapi
  :components
  ((:module "common"
    :components
    ((:file "package")
     (:file "base" :depends-on ("package"))
     (:file "panes" :depends-on ("base"))
     (:file "buttons" :depends-on ("panes"))
     (:file "interface" :depends-on ("panes"))))))
