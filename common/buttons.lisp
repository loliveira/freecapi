(in-package :freecapi)

(defclass simple-button (simple-pane mnemonic-text-mixin)
  ((button-group
    :initform nil
    :initarg :button-group
    :reader button-button-group)
   (interaction
    :initform :no-selection
    :initarg :interaction
    :reader button-interaction)
   (data
    :initform nil
    :initarg :data
    :accessor item-data)
   (key-function
    :initform 'identity
    :initarg :key-function
    :accessor button-key-function)
   (print-function
    :initform nil
    :initarg :print-function
    :accessor button-print-function)
   (image
    :initform nil
    :initarg :image
    :reader button-image)
   (disabled-image
    :initform nil
    :initarg :disabled-image
    :reader button-disabled-image)
   (armed-image
    :initform nil
    :initarg :armed-image
    :reader button-armed-image)
   (selected-image
    :initform nil
    :initarg :selected-image
    :reader button-selected-image)
   (selected-disabled-image
    :initform nil
    :initarg :selected-disabled-image
    :reader button-selected-disabled-image)
   (indicator
    :initform t
    :initarg :indicator
    :reader button-indicator)
   (text-alignment
    :initform nil
    :initarg :text-alignment
    :reader button-text-alignment))
  (:default-initargs :visible-max-height t
                     :visible-max-width t))

(defclass button (simple-button item)
  ((selection-callback :initarg :callback))
  (:default-initargs :print-function 'princ-to-string
                     :visible-max-width t))

(defclass user-button (titled-pane button)
  ()
  (:default-initargs :title-position :left))

(defclass push-button (user-button)
  ()
  (:default-initargs :interaction :no-selection))

