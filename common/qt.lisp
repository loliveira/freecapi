(in-package :freecapi)

(named-readtables:in-readtable :qt)

(defclass qt-backend ()
  ())

(defparameter *backend* (make-instance 'qt-backend))

(defmethod create-representation ((element library-element)
                                  (backend qt-backend))
  (warn "don't know how to represent ~S." element))

(defmethod create-representation ((object simple-button)
                                  (backend qt-backend))
  (let ((button (#_new QPushButton (item-data object))))
    (#_resize button 100 30)
    button))

(defmethod show-representation ((object library-element)
                                (backend qt-backend))
  (if (null (representation object))
      (warn "~S lacks a representation." object)
      (#_show (representation object))))

(defmethod contain ((object library-element))
  (show-representation object *backend*))
