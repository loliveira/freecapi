(in-package :freecapi)

(defclass capi-object ()
  ((name :initform nil :initarg :name :accessor capi-object-name)
   (plist :initform nil :initarg :plist :accessor capi-object-plist)))

(defclass basic-element (capi-object)
  ((parent :initform nil :initarg :parent :reader element-parent)))

(defclass simple-element (basic-element)
  ((geometry-cache :initform nil :accessor geometry-cache)
   (hint-table :initform nil :accessor hint-table)
   (accepts-focus-p :initform t :initarg :accepts-focus-p)
   (help-key :initform t :initarg :help-key :reader help-key)))

(defclass library-element (capi-object)
  ((representation :initform nil :accessor representation)))

(defmethod intialize-instance :after ((el library-element) &key)
  (setf (representation el) (create-representation el *representation*)))

(defclass element (simple-element library-element)
  ((interface :initform nil :initarg :interface :accessor element-interface)
   (updates :initform nil :accessor element-updates)))

(defclass decorated-object ()
  ((decoration :initform nil :accessor decoration)))

(defclass titled-object (decorated-object)
  ())

(defclass mnemonic ()
  ((mnemonic-index :initform nil :reader mnemonic-index)
   (mnemonic-default-p :initform nil :accessor mnemonic-default-p)))

(defclass mnemonic-text-mixin (mnemonic)
  ())

(defclass callbacks (capi-object)
  ((selection-callback :initform nil :initarg :selection-callback
                       :accessor callbacks-selection-callback)
   (extend-callback :initform nil :initarg :extend-callback
                    :accessor callbacks-extend-callback)
   (retract-callback :initform nil :initarg :retract-callback
                     :accessor callbacks-retract-callback)
   (action-callback :initform nil :initarg :action-callback
                    :accessor callbacks-action-callback)
   (callback-type :initform nil :initarg :callback-type
                  :accessor callbacks-callback-type)))

(defclass item (callbacks capi-object)
  ((data :initform nil :initarg :data :accessor item-data)
   (text :initform nil :initarg :text :accessor item-text)
   (selected :initform nil :initarg :selected :reader item-selected)
   (print-function :initform nil :initarg :print-function
                   :accessor item-print-function)
   (data-function :initform 'identity :initarg :data-function)
   (collection :initform nil :initarg :collection :accessor item-collection)))
