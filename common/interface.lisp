(in-package :freecapi)

(defparameter *auto-menus* t)

(defclass basic-interface (capi-object)
  ())

(defclass interface (pane-with-layout basic-interface titled-object)
  ((title
    :initform nil
    :initarg :title
    :accessor interface-title)
   (title-font
    :initform nil
    :initarg :title-font
    :accessor interface-title-font)
   (message
    :initform ""
    :initarg :message
    :accessor titled-object-message)
   (message-area
    :initform nil
    :initarg :message-area
    :accessor interface-message-area)
   (panes
    :initform nil
    :accessor interface-panes)
   (layouts
    :initform nil
    :reader interface-layouts)
   (menus
    :initform nil
    :accessor interface-menus)
   (auto-menus
    :initform *auto-menus*
    :initarg :auto-menus
    :reader interface-auto-menus)
   (menu-bar-items
    :initform nil
    :initarg :menu-bar-items
    :reader interface-menu-bar-items)
   (menu-bar-status
    :initform nil)
   (override-cursor
    :initform nil
    :initarg :override-cursor
    :reader interface-override-cursor)
   (busy-interface-state-stack
    :initform nil
    :accessor interface-busy-interface-state-stack)
   (tooltips-enabled
    :initform t
    :initarg :enable-tooltips
    :accessor interface-tooltips-enabled)
   (pointer-documentation-enabled
    :initform t
    :initarg :enable-pointer-documentation
    :accessor interface-pointer-documentation-enabled)
   (help-callback
    :initform nil
    :initarg :help-callback
    :accessor interface-help-callback)
   (confirm-destroy-function
    :initform nil
    :initarg :confirm-destroy-callback
    :initarg :confirm-destroy-function
    :accessor interface-confirm-destroy-function)
   (destroy-callback
    :initform nil
    :initarg :destroy-callback
    :accessor interface-destroy-callback)
   (create-callback
    :initform nil
    :initarg :create-callback
    :accessor interface-create-callback)
   (activate-callback
    :initform nil
    :initarg :activate-callback
    :accessor interface-activate-callback)
   (iconize-callback
    :initform nil
    :initarg :iconify-callback
    :initarg :iconize-callback
    :accessor interface-iconify-callback
    :accessor interface-iconize-callback)
   (geometry-change-callback
    :initform nil
    :initarg :geometry-change-callback
    :accessor interface-geometry-change-callback)
   (navigation-table
    :initform :uninitialized
    :accessor interface-navigation-table)
   (accelerator-table
    :initform :uninitialized
    :accessor interface-accelerator-table)
   (execution-data
    :initform nil
    :accessor interface-execution-data)
   (process
    :initform nil
    :accessor interface-process)
   (top-level-hook
    :initform nil
    :initarg :top-level-hook
    :accessor interface-top-level-hook)
   (external-border
    :initform 0
    :initarg :external-border
    :accessor top-level-interface-external-border)
   (created-state
    :initform nil
    :accessor interface-created-state)))

(defmethod print-object ((object interface) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (write-string (interface-title object) stream)))
