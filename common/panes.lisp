(in-package :freecapi)

(defclass simple-pane (element)
  ((foreground
    :initform nil
    :initarg :foreground
    :accessor simple-pane-foreground)
   (background
    :initform nil
    :initarg :background
    :accessor simple-pane-background)
   (font
    :initform nil
    :initarg :font
    :accessor simple-pane-font)
   (cursor
    :initform nil
    :initarg :cursor
    :accessor simple-pane-cursor)
   (enabled
    :initform t
    :initarg :enabled
    :reader simple-pane-enabled)
   (horizontal-scroll
    :initform nil
    :initarg :horizontal-scroll
    :reader simple-pane-horizontal-scroll)
   (vertical-scroll
    :initform nil
    :initarg :vertical-scroll
    :reader simple-pane-vertical-scroll)
   (scroll-callback
    :initform nil
    :initarg :scroll-callback
    :accessor simple-pane-scroll-callback)
   (internal-border
    :initform nil
    :initarg :internal-border
    :reader simple-pane-internal-border)
   (visible-border
    :initform :default
    :initarg :visible-border
    :reader simple-pane-visible-border)
   (pane-menu
    :initform :default
    :initarg :pane-menu
    :accessor pane-menu)
   (resource-name
    :initform nil
    :allocation :class
    :reader simple-pane-resource-name)
   (flags
    :initform 0)
   (color-requirements
    :initform nil
    :initarg :color-requirements
    :accessor simple-pane-color-requirements)
   (drop-callback
    :initform nil
    :initarg :drop-callback
    :accessor simple-pane-drop-callback)))

(defmethod initialize-instance :after ((pane simple-pane)
                                       &key visible-max-width visible-max-height)
  (warn "ignoring :VISIBLE-MAX-{WIDTH,HEIGHT} for ~S." pane))

(defclass titled-pane (simple-pane titled-object)
  ())

(defmethod initialize-instance :after ((pane titled-pane) &key title-position)
  (warn "ignoring :TITLE-POSITION for ~S." pane))

(defclass pane-with-children (simple-pane)
  ((created
    :initform nil)
   (initial-focus
    :initform nil
    :initarg :initial-focus
    :reader initial-focus
    :reader pane-initial-focus)
   (force-window-handle
    :initform nil
    :initarg :force-window-handle))
  (:default-initargs :help-key nil))

(defclass pane-with-layout (pane-with-children)
  ((layout :initform nil :initarg :layout :reader pane-layout))
  (:default-initargs :visible-border nil))
